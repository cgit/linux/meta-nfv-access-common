IMAGE_FEATURES[validitems] += "empty-root-password allow-empty-password post-install-logging "

IMAGE_FEATURES += " \
                   dbg-pkgs \
                   debug-tweaks \
                   dev-pkgs \
                   doc-pkgs \
                   staticdev-pkgs \
                   eclipse-debug \
                   nfs-server \
                   ssh-server-openssh \
                   tools-debug \
                   tools-profile \
                   tools-sdk \
                   "
SDKIMAGE_FEATURES += " \
                      dev-pkgs \
                      dbg-pkgs \
                      staticdev-pkgs \
                      "

IMAGE_INSTALL += " \
    binutils \
    dhcp-client \
    fuse \
    gnutls \
    kernel-dev \
    kernel-modules \
    kernel-vmlinux \
    lsb \
    lsbinitscripts \
    mingetty \
    pkgconfig \
    "
