MIRROR_ENEA_DISTRO = "${@d.getVar('DISTRO_NAME').replace(" ","")}${@d.getVar('DISTRO_VERSION_MAJOR').replace(" ","")}${@d.getVar('DISTRO_VERSION_MINOR').replace(" ","")}"

MIRRORS_append = "\
cvs://.*/.*     http://nfv-access-mirror/${MIRROR_ENEA_DISTRO}/sources/ \n \
svn://.*/.*     http://nfv-access-mirror/${MIRROR_ENEA_DISTRO}/sources/ \n \
git://.*/.*     http://nfv-access-mirror/${MIRROR_ENEA_DISTRO}/sources/ \n \
hg://.*/.*      http://nfv-access-mirror/${MIRROR_ENEA_DISTRO}/sources/ \n \
bzr://.*/.*     http://nfv-access-mirror/${MIRROR_ENEA_DISTRO}/sources/ \n \
p4://.*/.*      http://nfv-access-mirror/${MIRROR_ENEA_DISTRO}/sources/ \n \
osc://.*/.*     http://nfv-access-mirror/${MIRROR_ENEA_DISTRO}/sources/ \n \
https?$://.*/.* http://nfv-access-mirror/${MIRROR_ENEA_DISTRO}/sources/ \n \
ftp://.*/.*     http://nfv-access-mirror/${MIRROR_ENEA_DISTRO}/sources/ \n \
"
