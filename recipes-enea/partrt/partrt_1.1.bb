SUMMARY = "CPU partitioning tool"
DESCRIPTION = "partrt is a tool for dividing a SMP Linux system into a real time domain and a non-real time domain."
SECTION = "utils"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b52bab7a403562f36be803f11489f1a4"

RDEPENDS_${PN} = "bash bitcalc"
RDEPENDS_${PN}-ptest += "python3"

SRC_URI = "git://github.com/OpenEneaLinux/rt-tools.git;branch=master \
           file://run-ptest \
           "

SRCREV = "b46c72324c19cf74730ad562fc3f050d31018034"

inherit ptest

S = "${WORKDIR}/git"

do_install() {
        install -m 0755 -D ${S}/partrt/partrt ${D}${bindir}/partrt
}

do_install_ptest() {
        install ${S}/partrt/test/test_partition.py ${D}${PTEST_PATH}
        sed -i s/target/${MACHINE}/ ${D}${PTEST_PATH}/run-ptest
}
