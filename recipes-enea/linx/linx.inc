SUMMARY = "LINX for Linux fast IPC"
DESCRIPTION = "LINX is a distributed communication protocol stack for transparent inter node and interprocess communication for a heterogeneous mix of systems."
HOMEPAGE = "http://linx.sourceforge.net/"
SECTION = "system/library"

DEPENDS = "linux-libc-headers virtual/libc"
RRECOMMENDS_${PN} = "linx-mod"

SRC_URI = "http://linux.enea.com/linx/linx-${PV}.tar.gz"

SRC_URI[md5sum] = "f9d7634faa2d7338e51418e2dca82875"
SRC_URI[sha256sum] = "d047eb8d4b63ae385bf89dc3cc09854a0fe27f3f51c19dc332da7d40f9c0c28c"

S = "${WORKDIR}/linx-${PV}"

inherit autotools-brokensep

do_install_append () {
        mkdir -p ${D}/etc
        install -m 644 ${S}/linxgw/linxgws/example.conf ${D}/etc/linxgws.conf
}

