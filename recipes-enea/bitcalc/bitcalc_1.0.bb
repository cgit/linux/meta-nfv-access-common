SUMMARY = "Translate CPU list to a CPU mask"
DESCRIPTION = "Translate CPU list given on command line to a hexadecimal CPU mask. Can use kernel boot parameters isolcpus or nohz_full as input as well as a list given on command line."
SECTION = "utils"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b52bab7a403562f36be803f11489f1a4"

PR = "r0"

DEPENDS = "gcc-sanitizers"
RDEPENDS_${PN} = "bash"

FILESEXTRAPATHS_append := "${THISDIR}/${PN}"

SRC_URI = "git://github.com/OpenEneaLinux/rt-tools.git;branch=master \
           file://0001-bitcalc-resolved-compilation-warnings.patch \
           file://0001-bitcalc-resolved-memory-leaks.patch \
           "

DEPENDS_remove_aarch64 = "gcc-sanitizers"
SRC_URI_append_aarch64 = "file://0001-bitcalc-disable-gcc-address-sanitizer.patch"

SRCREV = "b46c72324c19cf74730ad562fc3f050d31018034"

S = "${WORKDIR}/git"

EXTRA_OEMAKE += "'DESTDIR=${D}${bindir}'"

inherit pkgconfig cmake

do_install() {
        install -m 0755 -D ${B}/bitcalc/src/bitcalc ${D}${bindir}/bitcalc
}
