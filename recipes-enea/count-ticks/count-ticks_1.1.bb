SUMMARY = "Tick count tool"
DESCRIPTION = "Count number of kernel ticks during command execution."
SECTION = "utils"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b52bab7a403562f36be803f11489f1a4"

PR = "r1"

RDEPENDS_${PN} = "bash"

SRC_URI = "git://github.com/OpenEneaLinux/rt-tools.git;branch=master \
           file://run-ptest \
           "

SRCREV = "b46c72324c19cf74730ad562fc3f050d31018034"

inherit ptest

S = "${WORKDIR}/git"

do_install() {
        install -m 0755 -D ${S}/count_ticks/count_ticks ${D}${bindir}/count_ticks
}
