
do_install_append() {
    # Don't run 'dpkg --configure' for debs, because it is already executed by a
    # service included in the dpkg recipe
    if ${@bb.utils.contains('PACKAGE_CLASSES', 'package_deb', 'true', 'false', d)}; then
        sed -i 's/backend_list="rpm deb ipk"/backend_list="rpm ipk"/' \
            ${D}${sbindir}/run-postinsts
    fi

    # Don't call update-rc.d remove if systemd is present
    if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
        sed -i "s/update-rc.d -f run-postinsts remove/:/" ${D}${sbindir}/run-postinsts
    fi
}
